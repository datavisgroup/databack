package com.capstone.databack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabackApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatabackApplication.class, args);
    }

}
