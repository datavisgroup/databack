package com.capstone.databack.repository;

import java.util.Optional;

import com.capstone.databack.model.Role;
import com.capstone.databack.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
