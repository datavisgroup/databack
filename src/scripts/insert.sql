

INSERT IGNORE INTO role_names (
  id,
  name
) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_PM'),
(3, 'ROLE_ADMIN');

INSERT IGNORE INTO roles (
  id,
  name
) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_PM'),
(3, 'ROLE_ADMIN');
